"use strict";
customPopup.service('customPopUpService',['$modal','$modalStack',function ($modal,$modalStack) {

    return({
        openCustomAlert:openCustomAlert,
        closeCustomAlert:closeCustomAlert
    });


    function openCustomAlert(alertObject) {
         var modalInstanceObject={
             templateUrl: 'app/modules/shared/popUp/customPopUp.html',
             controller: 'customAlertCtrl',
             size: 'sm',
             backdrop:true,

             resolve: {
                 items: function () {
                     return alertObject;
                 }
             }
         };

        if(alertObject.compulsory!=undefined && alertObject.compulsory==1){

            modalInstanceObject.backdrop='static';
            modalInstanceObject.windowClass='disableBckBtn';
        }





        var modalInstance = $modal.open(modalInstanceObject);

        modalInstance.result.then(function () {

        }, function () {
            // console.log("closed");
        });

    }
    function closeCustomAlert(){
        console.log($modalStack)
        $modalStack.dismissAll();
    }
}]);






