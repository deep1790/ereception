"use strict";
var customPopup = angular.module('customPopup',[]);

customPopup.controller('customAlertCtrl',['$scope', '$modalInstance', '$location','items','$sce',function ($scope, $modalInstance, $location,items,$sce) {
    if (items.cancelText != null)
        $scope.showCancel = true;
    if (items.OK_ButtonText != null)
        $scope.showOk = true;
    $scope.items = items;
    $scope.items.alertMessage= $sce.trustAsHtml($scope.items.alertMessage);
    $scope.ok = function () {
        $modalInstance.dismiss();
        items.OK_ButtonCallBack();

    };
    $scope.cancel = function () {
        items.cancelCallback();
        $modalInstance.dismiss();
    };
}]);

