var signUpModule = angular.module('signUp',[]);

signUpModule.config(['$stateProvider','$urlRouterProvider', function ($stateProvider,$urlRouterProvider) {
    $stateProvider.
        state('signUp', {
            url:'/signUp',
            templateUrl: 'app/modules/components/signUp/signUpView.html',
            controller: 'signUpCtrl'
        });
}]);

signUpModule.controller('signUpCtrl', ['$scope','communication','customPopUpService','config', function ($scope,communication,customPopUpService,config) {

    $scope.signUpFeilds={};
    $scope.signUpFeilds.fname='';
    $scope.signUpFeilds.lname='';
    $scope.signUpFeilds.email='';
    $scope.signUpFeilds.phno='';
    $scope.signUpFeilds.pwd='';



    $scope.signIUp = function () {
        if ($scope.isSignUpFormValid()) {
            /*     communication.POST({
             url: url.api('signUp'),
             data: $scope.signUpFeilds,
             successCallback: function (response) {
             },
             onCompleteCallBack: function () {
             },
             errorCallback: function () {

             }
             });*/
        }
    }
    
    
    $scope.isSignUpFormValid= function () {

        var emailRegEx = /\S+@\S+\.\S+/;
        var mobileNoRegEx = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;
        if($scope.signUpFeilds.fname.trim()=='' || $scope.signUpFeilds.lname.trim()=='' || $scope.signUpFeilds.email.trim()=='' || $scope.signUpFeilds.phno.trim()=='' || $scope.signUpFeilds.pwd.trim()=='' ){
            customPopUpService.openCustomAlert({
                alertHeaderText: config.messagesTitle.alert,
                alertMessage: config.messageSubject.emptyFieldsErrorMessage,
                OK_ButtonText: 'OK',
                cancelText: null,
                OK_ButtonCallBack: function () {
                },
                cancelCallback: function () {
                }
            });
        }else if($scope.signUpFeilds.email.match(emailRegEx) == null){
            customPopUpService.openCustomAlert({
                alertHeaderText: config.messagesTitle.alert,
                alertMessage: config.messageSubject.emailValidationError,
                OK_ButtonText: 'OK',
                cancelText: null,
                OK_ButtonCallBack: function () {
                },
                cancelCallback: function () {
                }
            });
        }
        else if($scope.signUpFeilds.phno.match(mobileNoRegEx) == null){
            customPopUpService.openCustomAlert({
                alertHeaderText: config.messagesTitle.alert,
                alertMessage: config.messageSubject.phnoValidationError,
                OK_ButtonText: 'OK',
                cancelText: null,
                OK_ButtonCallBack: function () {
                },
                cancelCallback: function () {

                }
            });
        }
    }


}]);