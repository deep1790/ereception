var loginModule = angular.module('login', []);

loginModule.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider.
        state('login', {
            url: '/login',
            templateUrl: 'app/modules/components/login/loginView.html',
            controller: 'loginCtrl'
        });
}]);

loginModule.controller('loginCtrl', ['$scope','$state', '$location','customPopUpService','communication','config', function ($scope,$state,$location,customPopUpService,communication,config) {
    $scope.loginFields = {};
    $scope.loginFields.name = '';
    $scope.loginFields.pwd = '';

    $scope.signIn = function () {
        if ($scope.isLoginFormValid()) {
       /*     communication.POST({
                url: url.api('login'),
                data: $scope.loginFields,
                successCallback: function (response) {
                },
                onCompleteCallBack: function () {
                },
                errorCallback: function () {

                }
            });*/
        }
    }

    $scope.isLoginFormValid= function () {
        if($scope.loginFields.name.trim()=='' || $scope.loginFields.pwd.trim()=='' ){
            customPopUpService.openCustomAlert({
                alertHeaderText: config.messagesTitle.alert,
                alertMessage: config.messageSubject.emptyFieldsErrorMessage,
                OK_ButtonText: 'OK',
                cancelText: null,
                OK_ButtonCallBack: function () {
                },
                cancelCallback: function () {
                }
            });
        }
        else{
            return true;
        }
    }
    $scope.registerNow = function () {
        $state.go('signUp')
       // $location.path('/signUp')
    }
}]);