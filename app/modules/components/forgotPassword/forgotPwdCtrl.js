var forgotPwdModule = angular.module('forgotPwd', []);

forgotPwdModule.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider.
        state('forgotPwd', {
            url: '/forgotPwd',
            templateUrl: 'app/modules/components/forgotPassword/forgotPwdView.html',
            controller: 'forgotPwdCtrl'
        });
}]);

forgotPwdModule.controller('forgotPwdCtrl', ['$scope', function ($scope) {

}]);
