var healthCare = angular.module('healthCare', ['ui.router','ngAnimate','anim-in-out','ui.bootstrap','login','signUp','customPopup','forgotPwd']);

healthCare.config(['$stateProvider','$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.
        otherwise('/login');

}]).run(['$rootScope',function ($rootScope) {


}]);
