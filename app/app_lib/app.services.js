healthCare.factory('communication',['$http','$rootScope' ,function ($http, $rootScope) {

    $http.defaults.withCredentials = false;
    $http.defaults.useXDomain = true;
    delete $http.defaults.headers.common['X-Requested-With'];
    $rootScope.loading = true;

    return ({
        GET: GET,
        POST: POST
    });

    /**
     * Name    : GET
     * Purpose : Method for making the GET request service call.
     * successCallback  - On Success, the control will be given back to the function
     * errorCallback - On Error, the control will be given back to the function,
     * Returns : --
     **/
    function GET(options) {


        $http({
            url: options.url,
            method: "GET",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {
            options.successCallback(response);
        }).error(function (response) { // optional
                options.errorCallback(response);
            }
        );

    }


    /**
     * Name    : POST
     * Purpose : Method for making the POST request service call
     * data - Request body to be sent to the server
     * successCallback  - On Success, the control will be given back to the function
     * errorCallback - On Error, the control will be given back to the function,
     * Returns : --
     **/
    function POST(options) {

        console.log(options.data);
        /*$http({
         url: options.url,
         method: "POST",
         data: JSON.stringify(options.data),
         headers: {contentType : "application/json; charset=utf-8",
         'Cache-Control': 'no-cache',
         dataType: "json"
         }
         }).success(function (response) {
         options.successCallback(response);
         });*/

        $http({
            url:options.url,
            method: "POST",
            data: options.data,
            headers: {'Content-Type': 'application/json',
                'Cache-Control':'no-cache',
            }
        })
            .then(function (response) {
                options.successCallback(response);
            },
            function (response) { // optional

            }
        );


    }
}]);


healthCare.factory('url',['config',function (config) {
    return{
        api:api
    }
    function api(apiString){
        return config.apiBaseUrl +config.urls[apiString];
    }
}]);