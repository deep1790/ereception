/**
 * Name    : settings
 * Purpose : Local App Settings.
 * Params  : --
 * Returns : --
 **/

healthCare.constant('config', {
    apiBaseUrl : "http://mdata-staging.yebhi.com",
    urls: {
        login : '/login',
        signup : '/register'
    },
    messagesTitle: {
        alert:'Alert !!!'
    },
    messageSubject: {
        emptyFieldsErrorMessage : "Please fill these details.",
        emailValidationError:"Please fill valid eamil.",
        phnoValidationError:"Please fill valid mobile number."
    },
    messageTitle: {
        sorry : "Sorry"
    }
});